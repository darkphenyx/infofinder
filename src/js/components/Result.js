/**
 * Created by joshua.fair on 7/15/2016.
 */
/**
 * Created by joshua.fair on 7/15/2016.
 */
var React = require('react');
var AppActions = require('../actions/AppActions');
var AppStore = require('../stores/AppStore');

var Result = React.createClass({
    render: function () {
        return(
            <div>
                <p className="content lead well" dangerouslySetInnerHTML={{__html:this.props.result.Result}}></p>
            </div>
        )
    }
});

module.exports = Result;