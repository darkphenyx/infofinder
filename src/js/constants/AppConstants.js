/**
 * Created by joshua.fair on 7/13/2016.
 */
module.exports = {
    SEARCH_TEXT: 'SEARCH_TEXT',
    RECEIVE_RESULTS: 'RECEIVE_RESULTS'
}